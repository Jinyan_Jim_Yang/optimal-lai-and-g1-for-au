source("R/process-compare2soil.R")

library(mgcv)

fit.site <- gam(measured.mean~s(VPD.mean)+s(MAP)+s(PAR.mean)+s(TMAX.mean),data=result.sumall)
fit.site.nu <- gam(measured.mean~s(VPD.mean)+s(MAP)+s(PAR.mean)+s(TMAX.mean)+s(P.mean)+s(N.mean),
                   data=result.sumall)
# summary(bp)
# plot(bp)

fit.modis <- gam(modis.mean~s(MAP)+s(VPD.mean)+s(PAR.mean)+s(TMAX.mean),data=result.sumall)
# summary(fit.modis)
# plot(bp.2)

# plot(modis.mean ~ N.mean,data = result.sumall)

map.de.m <- readRDS("cache/map.sub.de.rds")
vpd.de.m <- readRDS("cache/vpd.sub.de.rds")
par.de.m <- readRDS("cache/par.sub.de.rds")
tmax.de.m <- readRDS("cache/tmax.sub.de.rds")
modis.de.m <- readRDS("cache/modis.sub.de.rds")

modis <- as.vector(modis.de.m)
map <- as.vector(map.de.m)
vpd <- as.vector(vpd.de.m)
par <- as.vector(par.de.m)
tmax <- as.vector(tmax.de.m)

fit.modis.au <- gam(modis~s(map)+s(vpd)+s(par)+s(tmax))

# summary(fit.modis.au)

out.1 <- capture.output(summary(fit.site))
out.1.1 <- capture.output(summary(fit.site.nu))
out.2 <- capture.output(summary(fit.modis))
out.3 <- capture.output(summary(fit.modis.au))
 
cat("****Summary of GAM of measured lai vs climate****", out.1, file="summary_of_GAM.txt", sep="\n", append=FALSE)
cat("\n\n\n\n****Summary of GAM of measured lai vs climate and nutirents****", out.1.1, file="summary_of_GAM.txt", sep="\n", append=TRUE)
cat("\n\n\n\n****Summary of GAM of in situ modis vs climate****", out.2, file="summary_of_GAM.txt", sep="\n", append=TRUE)
cat("\n\n\n\n****Summary of GAM of continent modis vs climate****", out.3, file="summary_of_GAM.txt", sep="\n", append=TRUE)

# predict with model
# Predicted LAI lai vs reported
lai.reg <- predict(fit.site,data.frame(MAP=result.sumall$MAP,VPD.mean=result.sumall$VPD.mean,
                                       PAR.mean=result.sumall$PAR.mean,TMAX.mean=result.sumall$TMAX.mean))
plot.gam.func <- function(out.fn){
  pdf(file.path("output/figures",out.fn),width = 10,height = 7)
  on.exit(dev.off())
  # model reg vs reported
plot(lai.reg ~ result.sumall$measured.mean,pch=16,col="red",
     xlim=c(0,7),ylim=c(0,7),xlab="",ylab="")
abline(a=0,b=1,lty="dashed",col="black")
abline(lm(lai.reg ~ result.sumall$measured.mean),lty="solid",col="red")
par(new=TRUE)
plot(result.sumall$LAI.mean ~ result.sumall$measured.mean,pch=16,col="blue",
     xlim=c(0,7),ylim=c(0,7),xlab="Reported",ylab="Predicted LAI")
abline(lm(result.sumall$LAI.mean ~ result.sumall$measured.mean),lty="solid",col="blue")
legend("topleft",legend = c("Modelled","GAM"),pch=16,col=c("blue","red"),bty='n')

# model vs reg
plot(lai.reg ~ result.sumall$LAI.mean,pch=16,col="red",
     xlim=c(0,7),ylim=c(0,7),xlab="Modelled LAI",ylab="GAM LAI")
abline(a=0,b=1,lty="dashed",col="coral")

# over climate factors
# map
plot(lai.reg ~ result.sumall$MAP,pch=16,col="red",
     ylim=c(0,7),xlab="",ylab="")
par(new=TRUE)
plot(result.sumall$LAI.mean ~ result.sumall$MAP,pch=16,col="blue",
     ylim=c(0,7),xlab="MAP",ylab="Predicted LAI")

legend("topleft",legend = c("Modelled","GAM"),pch=16,col=c("blue","red"),bty='n')
# vpd
plot(lai.reg ~ result.sumall$VPD.mean,pch=16,col="red",
     ylim=c(0,7),xlab="",ylab="")
par(new=TRUE)
plot(result.sumall$LAI.mean ~ result.sumall$VPD.mean,pch=16,col="blue",
     ylim=c(0,7),xlab="VPD",ylab="Predicted LAI")

legend("topleft",legend = c("Modelled","GAM"),pch=16,col=c("blue","red"),bty='n')

# par
plot(lai.reg ~ result.sumall$PAR.mean,pch=16,col="red",
     ylim=c(0,7),xlab="",ylab="")
par(new=TRUE)
plot(result.sumall$LAI.mean ~ result.sumall$PAR.mean,pch=16,col="blue",
     ylim=c(0,7),xlab="PAR",ylab="Predicted LAI")

legend("topleft",legend = c("Modelled","GAM"),pch=16,col=c("blue","red"),bty='n')

# Tmax
plot(lai.reg ~ result.sumall$TMAX.mean,pch=16,col="red",
     ylim=c(0,7),xlab="",ylab="")
par(new=TRUE)
plot(result.sumall$LAI.mean ~ result.sumall$TMAX.mean,pch=16,col="blue",
     ylim=c(0,7),xlab="TMAX",ylab="Predicted LAI")

legend("topleft",legend = c("Modelled","GAM"),pch=16,col=c("blue","red"),bty='n')

}

plot.gam.func("gamPlot.pdf")
