source("load.R")
# note about Ellis' data:
# Should be ecosystem LAI 

# Ellis data
ellis.data <- read.csv("Ellis 2008.csv",header = FALSE)
# correct typo in the paper
ellis.data$V15[37] <- 1180
ellis.data$V5 <- gsub("\002", "", ellis.data$V5)
ellis.data$V5 <- as.numeric(ellis.data$V5)

# fit regression as they did
fit.ellis <- lm(V17~V15,data = ellis.data) 

# coord for vpd
lati <- ncvar_get(nc_open("downloads/pavg/pavg2002.nc"), "latitude")
long <- ncvar_get(nc_open("downloads/pavg/pavg2002.nc"), "longitude")

# get cordination
deci.s <- ellis.data$V6/60
deci.w <- ellis.data$V8/60
ellis.data$Latitude <- -(ellis.data$V5 + deci.s)
ellis.data$Longitude <- ellis.data$V7 + deci.w
coord.s <- ellis.data$Latitude 
coord.w <- ellis.data$Longitude 

# find closest value
num.lati <- c()
for (i in 1:length(coord.s)){
  num.lati[i] <- which(abs(lati-coord.s[i]) == min(abs(lati - coord.s[i])))
}

num.longi <- c()
for (i in 1:length(coord.s)){
  num.longi[i] <- which(abs(long-coord.w[i]) == min(abs(long - coord.w[i])))
}

# simulated ellis
#use 20 year mean climate data
vpd.ellis<-c()
VPD.20.mean <- readRDS(file.path("cache","vpd1991_2011.rds"))
for (i in 1:length(num.longi)){
  vpd.ellis[i] <- VPD.20.mean[num.longi[i],num.lati[i]]
}
par.ellis<-c()
par.20.mean <- readRDS(file.path("cache","par1991_2011.rds"))
for (i in 1:length(num.longi)){
  par.ellis[i] <- par.20.mean[num.longi[i],num.lati[i]]
}
tmax.ellis <- c()
tmax.20.mean <- readRDS(file.path("cache","tmax1991_2011.rds"))
for (i in 1:length(num.longi)){
  tmax.ellis[i] <- tmax.20.mean[num.longi[i],num.lati[i]]
}

rain.ellis <- c()
rain.20.mean <- readRDS(file.path("cache","pavg1991_2011.rds"))
for (i in 1:length(num.longi)){
  rain.ellis[i] <- rain.20.mean[num.longi[i],num.lati[i]]
}

# tmin.ellis <- c()
# tmin.20.mean <- readRDS(file.path("cache","tmin1991_2011.rds"))
# for (i in 1:length(num.longi)){
#   tmin.ellis[i] <- tmin.20.mean[num.longi[i],num.lati[i]]
# }

result.ellis <- lai_ppt.func(vpd = vpd.ellis,
                           rain.test = rain.ellis,
                           par.vec = par.ellis,
                           tmax =tmax.ellis)
# result.ellis$tmin <- tmin.ellis
# result.ellis.both <- g1.lai_ppt.func(vpd = vpd.ellis,
#                                      rain.test = rain.ellis,
#                                      par.vec = par.ellis,
#                                      tmax =tmax.ellis)
# # 
# # make up a n up take rate based on rainfellis
# n.ellis <- 12/(1+exp(-0.002*(rain.ellis-1000)))
# result.ellis.both.n <- g1.lai_ppt.func(vpd = vpd.ellis,
#                                       rain.test = rain.ellis,
#                                       par.vec = par.ellis,
#                                       tmax = tmax.ellis,
#                                       n.up = n.ellis)

# result.ellis.both[grep("Error in integrate",result.ellis.both$g1),] <- rep(NA,ncol(result.ellis.both))

# var.n <- names(result.ellis.both)
# 
# for (i in seq_len(length(var.n))){
#   result.ellis.both[,i] <- as.numeric(as.character(result.ellis.both[,i]))
# }

# get modis for comparasion
modis.raw <- t(get.modis.mean())
modis <- modis.raw * 0.1

# and get the data to align with our outputs
long.modis <- readMat("modis/long.mat")$long
lati.modis <- readMat("modis/lati.mat")$lati
lati <- ncvar_get(nc_open("downloads/pavg/pavg2002.nc"), "latitude")
long <- ncvar_get(nc_open("downloads/pavg/pavg2002.nc"), "longitude")

col.lati <- c()
for (i in 1:length(lati)){
  col.lati[i] <- which(abs(lati.modis-lati[i])==min(abs(lati.modis-lati[i])))
}

row.long <- c()
for (i in 1:length(long)){
  row.long[i] <- which(abs(long.modis-long[i])==min(abs(long.modis-long[i])))
}

modis.sub <- modis[row.long,col.lati]
modis.ellis <- c()
for (i in 1:length(num.longi)){
  modis.ellis[i] <- modis.sub[num.longi[i],num.lati[i]]
}

# 
map.paper <- ellis.data$V15

result.mapInPaper <- lai_ppt.func(vpd = vpd.ellis,
                             rain.test = map.paper,
                             par.vec = par.ellis,
                             tmax =tmax.ellis)

lai.ellis <- ellis.data$V17
# make data set
result.ellis$modis <- modis.ellis
result.ellis$measured <- lai.ellis
result.ellis$map.paper <- map.paper
result.ellis$lai.rainInPaper <- result.mapInPaper$LAI
result.ellis$rain.group <- cut(result.ellis$map.paper,breaks=seq(200,3600,200),
                             labels = paste0(seq(200,3500,200)))
result.ellis$vpd.group <- cut(result.ellis$VPD,breaks=seq(0,4,2),
                            labels=paste0(seq(1,3,2)))

saveRDS(result.ellis,file = "output/data/result.ellis.rds")
# saveRDS(result.ellis.both,file = "output/data/result.ellis.both.rds")
# saveRDS(result.ellis.both.n,file = "output/data/result.ellis.both.n.rds")

result.ellis <- readRDS("output/data/result.ellis.rds")
# result.ellis.both <- readRDS("output/data/result.ellis.both.rds")
# result.ellis.both.n <- readRDS("output/data/result.ellis.both.n.rds")

# summary by plot
# r.e.sumed <- summaryBy(LAI + modis + measured ~ PPT,data=result.ellis,FUN = mean,na.rm=TRUE)

# result.sum <- summaryBy(LAI + lai.rainInPaper + measured + modis + MAP + map.paper + VPD ~ rain.group + vpd.group,
#                         data = result.ellis,FUN = c(mean,sd),na.rm=TRUE)
# result.sum <- na.omit(result.sum)
# 
# # plot summary with sd ()
# plot(result.sum$lai.rainInPaper.mean ~ result.sum$map.paper.mean,ylim=c(0,7),col="black",
#      cex=0.7,pch=16,xlab=" ",ylab=" ")
# arrows(result.sum$map.paper.mean, result.sum$lai.rainInPaper.mean-result.sum$lai.rainInPaper.sd, 
#        result.sum$map.paper.mean, result.sum$lai.rainInPaper.mean+result.sum$lai.rainInPaper.sd, 
#        length=0.05, angle=90, code=3,col="black")
# par(new=TRUE)
# plot(result.sum$measured.mean ~ result.sum$map.paper.mean,ylim=c(0,7),col="deepskyblue",
#      pch=17,cex=0.7,xlab="MAP",ylab="LAI")
# arrows(result.sum$map.paper.mean, result.sum$measured.mean - result.sum$measured.sd, 
#        result.sum$map.paper.mean, result.sum$measured.mean + result.sum$measured.sd, 
#        length=0.05, angle=90, code=3,col="deepskyblue")
# legend("bottomright", legend = c("Modelled","In situ"),pch=c(16,17),
#        col=c("black","deepskyblue"))
# title("Modelled LAI vs In situ data with SD")

# # plot summary with sd
# plot(result.sum$LAI.mean ~ result.sum$map.mean,xlim=c(200,3800),ylim=c(0,7),col="black",
#      cex=0.7,pch=16,xlab=" ",ylab=" ")
# arrows(result.sum$map.mean, result.sum$LAI.mean-result.sum$LAI.sd, 
#        result.sum$map.mean, result.sum$LAI.mean+result.sum$LAI.sd, 
#        length=0.05, angle=90, code=3,col="black")
# par(new=TRUE)
# plot(result.sum$measured.mean ~ result.sum$map.mean,xlim=c(200,3800),ylim=c(0,7),col="deepskyblue",
#      pch=17,cex=0.7,xlab="MAP",ylab="LAI")
# arrows(result.sum$map.mean, result.sum$measured.mean - result.sum$measured.sd, 
#        result.sum$map.mean, result.sum$measured.mean + result.sum$measured.sd, 
#        length=0.05, angle=90, code=3,col="deepskyblue")
# legend("bottomright", legend = c("Modelled","In situ"),pch=c(16,17),
#        col=c("black","deepskyblue"))
# title("Modelled LAI vs In situ data with SD")

# # same thing for mdois
# plot(result.sum$lai.rainInPaper.mean ~ result.sum$map.paper.mean,ylim=c(0,7),col="black",
#      cex=0.7,pch=16,xlab=" ",ylab=" ")
# arrows(result.sum$map.paper.mean, result.sum$lai.rainInPaper.mean-result.sum$lai.rainInPaper.sd, 
#        result.sum$map.paper.mean, result.sum$lai.rainInPaper.mean+result.sum$lai.rainInPaper.sd, 
#        length=0.05, angle=90, code=3,col="black")
# par(new=TRUE)
# plot(result.sum$modis.mean ~ result.sum$map.paper.mean,ylim=c(0,7),col="coral",
#      pch=15,cex=0.7,xlab="MAP",ylab="LAI")
# arrows(result.sum$map.paper.mean, result.sum$modis.mean - result.sum$modis.sd, 
#        result.sum$map.paper.mean, result.sum$modis.mean + result.sum$modis.sd, 
#        length=0.05, angle=90, code=3,col="coral")
# legend("bottomright", legend = c("Modelled","MODIS"),pch=c(16,15),
#        col=c("black","coral"))
# title("Modelled LAI vs MODIS with SD")

# plot func
plot.ellis.func<- function(outname){
  on.exit(dev.off())
  figure.path <- "output/figures"
  figure.name <- file.path(figure.path, outname)
  pdf(figure.name)
  par(mfrow=c(1,1),mar=c(4,5,1,5))

#   # plot modis model and in situ
#   plot(modis.ellis~rain.ellis,ylim=c(0,10),col="red",pch=16,xlab=" ",ylab=" ",cex=0.7)
#   fit.modis <- lm(modis.ellis~rain.ellis)
#   abline(fit.modis,col="red",lty=5,lwd=0.5)
#   par(new=TRUE)
#   plot(lai.ellis~rain.ellis,ylim=c(0,10),col="coral",pch=16,ann=FALSE,cex=0.7)
#   fit.ellies <- lm(lai.ellis~rain.ellis)
#   abline(fit.ellies,col="coral",lty=5,lwd=0.5)
#   par(new=TRUE)
#   with(result.ellis,plot(LAI~PPT,type="p",pch=17,cex=0.7,ylim=c(0,10),col="deepskyblue",
#                          xlab="MAP (mm/yr)", ylab=expression(paste("LAI","  ","(m"^"2","/","m"^"2",")"))))
#   fit.model <- lm(LAI~PPT,data = result.ellis)
#   abline(fit.model,col="deepskyblue",lty=5,lwd=0.5)
#   legend("topleft",legend = c("MODIS","In situ","Modelled"),pch=c(16,16,17),col=c("red","coral","deepskyblue"))
  
  #  plot summary by plot
  with(r.e.sumed,plot(LAI.mean~PPT,pch=17,cex=0.8,col="deepskyblue",xlab=" ",ylab=" ",ylim=c(0,10)))
  abline(lm(r.e.sumed$LAI.mean ~ r.e.sumed$PPT),col="deepskyblue",lty="dashed")
  par(new=TRUE)
  with(r.e.sumed,plot(modis.mean~PPT,pch=16,cex=0.8,col="red",xlab=" ",ylab=" ",ylim=c(0,10)))
  abline(lm(r.e.sumed$modis.mean ~ r.e.sumed$PPT),col="red",lty="dashed")
  par(new=TRUE)
  with(r.e.sumed,plot(measured.mean~PPT,pch=16,cex=0.8,col="coral",xlab="MAP (mm/yr)",ylab="LAI",ylim=c(0,10)))
  abline(lm(r.e.sumed$measured.mean ~ r.e.sumed$PPT),col="coral",lty="dashed")
  legend("topleft",legend = c("MODIS","In situ","Modelled"),pch=c(16,16,17),col=c("red","coral","deepskyblue"))
  title("Mean of each plot")
  
  # see why two trends in model ouputs
  cut.vpd <- cut(result.ellis$vpd,breaks=2)
  palette(c("blue","red"))
  with(result.ellis,plot(LAI~PPT,type="p",pch=3,cex=0.7,ylim=c(0,10),col=cut.vpd,
                         xlab="MAP (mm/yr)", ylab=expression(paste("LAI","  ","(m"^"2","/","m"^"2",")"))))
  par(new=TRUE)
  with(result.ellis,plot(modis~PPT,type="p",pch=16,cex=0.7,ylim=c(0,10),col=cut.vpd,
                         xlab="MAP (mm/yr)", ylab=expression(paste("LAI","  ","(m"^"2","/","m"^"2",")"))))
  par(new=TRUE)
  with(result.ellis,plot(measured~PPT,type="p",pch=17,cex=0.7,ylim=c(0,10),col=cut.vpd,
                         xlab="MAP (mm/yr)", ylab=expression(paste("LAI","  ","(m"^"2","/","m"^"2",")"))))
  legend("topleft",levels(cut.vpd),fill=palette(),title="VPD (kPa)")
  legend("bottomright",legend = c("modeled","modis","in situ"),pch=c(3,16,17),col="black")
  
  # see the error over map
  plot(100*(modis.ellis)/result.ellis$measured~rain.ellis,ylim=c(0,800),col="coral",pch=16,cex=0.7,
       xlab="MAP (mm/yr)",ylab="Modeled / Measured %")
  par(new=TRUE)
  plot(100*(result.ellis$LAI)/result.ellis$measured~rain.ellis,ylim=c(0,800),col="deepskyblue",pch=17,cex=0.7,
       xlab=" ",ylab=" ")
  abline(h=0,lty="dashed")
  legend("topright",legend = c("Modelled / Measured","MODIS / Measured"),pch=c(17,16),
         col=c("deepskyblue","coral"))
  
  # see the error over tmax
  plot(100*(result.ellis$modis)/result.ellis$measured~result.ellis$tmax,ylim=c(0,800),col="coral",pch=16,cex=0.7,
       xlab="TMAX (Celsius)",ylab="Modeled / Measured %")
  par(new=TRUE)
  plot(100*(result.ellis$LAI)/result.ellis$measured~result.ellis$tmax,ylim=c(0,800),col="deepskyblue",pch=17,cex=0.7,
       xlab=" ",ylab=" ")
  abline(h=0,lty="dashed")
  abline(h=100,lty="dashed")
  legend("topright",legend = c("Modelled / Measured","MODIS / Measured"),pch=c(17,16),
         col=c("deepskyblue","coral"))
  
  # see the error over par
  plot(100*(result.ellis$modis)/result.ellis$measured~result.ellis$par,ylim=c(0,800),col="coral",pch=16,cex=0.7,
       xlab=expression("PAR (MJ m"^-2*" yr"^-1*")"),ylab="Modeled / Measured %")
  par(new=TRUE)
  plot(100*(result.ellis$LAI)/result.ellis$measured~result.ellis$par,ylim=c(0,800),col="deepskyblue",pch=17,cex=0.7,
       xlab=" ",ylab=" ")
  abline(h=0,lty="dashed")
  abline(h=100,lty="dashed")
  legend("topright",legend = c("Modelled / Measured","MODIS / Measured"),pch=c(17,16),
         col=c("deepskyblue","coral"))
  
  # see over pet(only ellies)
  plot(100*(result.ellis$LAI[seq_len(nrow(ellis.data))] / ellis.data$V17) ~ ellis.data$V16,
       xlab="PET (mm/yr)",ylab="Modeled / Measured %")
  abline(h=100,lty="dashed")
  title("only ellies")
  
  # over vpd (only ellies)
  plot(100*(result.ellis$LAI[seq_len(nrow(ellis.data))] / ellis.data$V17) ~ result.ellis$vpd[seq_len(nrow(ellis.data))],
       xlab="VPD (kPa)",ylab="Modeled / Measured %")
  abline(h=100,lty="dashed")
  title("only ellies")
  
  # see the error over VPD
  plot(100*(result.ellis$modis)/result.ellis$measured~result.ellis$vpd,ylim=c(0,800),col="coral",pch=16,cex=0.7,
       xlab=expression("VPD (kPa)"),ylab="Modeled / Measured %")
  par(new=TRUE)
  plot(100*(result.ellis$LAI)/result.ellis$measured~result.ellis$vpd,ylim=c(0,800),col="deepskyblue",pch=17,cex=0.7,
       xlab=" ",ylab=" ")
  abline(h=0,lty="dashed")
  abline(h=100,lty="dashed")
  legend("topright",legend = c("Modelled / Measured","MODIS / Measured"),pch=c(17,16),
         col=c("deepskyblue","coral"))
  
  # density plot of model - in situ
  plot(density((result.ellis$LAI - result.ellis$measured)/lai.ellis,na.rm = TRUE,bw = 1),
       ylab="Density",xlab=expression("Percentile of Difference (%)"),xlim=c(-4,4),xaxt='n',
       main="LAI: (Modelled - Measured) / Measured")
  axis(1,at=seq(-4,4),labels = paste0(seq(-400,400,100),"%"))
  abline(v=0,lty="dashed")

  # density plot of modis - in situ
  plot(density((result.ellis$modis-result.ellis$measured)/lai.ellis,na.rm = TRUE,bw = 1),
       ylab="Density ",xlab=expression("Percentile of Difference (%)"),xlim=c(-4,4),xaxt='n',
       main="LAI: (MODIS - Measured) / Measured")
  axis(1,at=seq(-4,4),labels = paste0(seq(-400,400,100),"%"))
  abline(v=0,lty="dashed")
  
  # modis ~in situ ~ model
  plot(result.ellis$LAI~result.ellis$measured,xlim=c(0,10),ylim=c(0,10))
  abline(a=0,b=1)  
  plot(result.ellis$measured~result.ellis$modis,xlim=c(0,10),ylim=c(0,10))
  abline(a=0,b=1)
  plot(result.ellis$LAI~result.ellis$modis,xlim=c(0,10),ylim=c(0,10))
  abline(a=0,b=1)
  
#   #different  model comparision
#   lai.range <- c(0,ceiling(max(c(result.ellis.both.n$LAI,result.ellis.both$LAI,result.ellis$LAI),na.rm = TRUE)))
#   map.range <- ceiling(range(result.ellis$PPT))
#   with(result.ellis,plot(LAI~PPT,type="p",pch=20,cex=0.7,ylim=lai.range,xlim=map.range,
#                          col="deepskyblue",xlab=" ", ylab=" "))
#   par(new=TRUE)
#   with(result.ellis.both.n,plot(LAI~PPT,type="p",pch=15,cex=0.5,ylim=lai.range,xlim=map.range,
#                          col="coral",xlab=" ", ylab=" "))
#   par(new=TRUE)
#   with(result.ellis.both,plot(LAI~PPT,type="p",pch=17,cex=0.7,ylim=lai.range,xlim=map.range,
#                               col="burlywood",xlab="MAP (mm/yr)", 
#                               ylab=expression(paste("LAI","  ","(m"^"2","/","m"^"2",")"))),
#        main="model comparision")
#   
#   abline(lm(lai.ellis~rain.ellis))
#   legend("topleft",
#          legend = c("Optimise g1","Optimise both g1 and lai with fixed N",
#                     "Optimise both g1 and lai with N(MAP)","regression of data"),
#          pch=c(20,15,17,15),col=c("deepskyblue","coral","burlywood","black"),cex=0.7)
  
  # plot the emast rain vs rain in paper
  plot(result.ellis$map~result.ellis$map.paper,xlim=c(200,4000),ylim=c(200,4000))
  abline(a=0,b=1,lty="dashed")
  abline(lm(result.ellis$map~result.ellis$map.paper),col="coral")
  legend("bottomright",legend = c("1:1","Regression"),lty = c("dashed","solid"),col = c("black","coral"))
  
  # see difference between rain in literature and emast mean 
  plot((result.ellis$map-result.ellis$map.paper)/result.ellis$map.paper~result.ellis$map.paper)
  abline(h=0,lty="dashed")
  abline(lm((result.ellis$map-result.ellis$map.paper)/result.ellis$map.paper~result.ellis$map.paper),col="coral")
}

# plot.ellis.func("in situ vs model.pdf")

# # vpd based on tmax and tmin following Zotarelli et al 2015
# t.max <- ellis.data$V13
# t.min <- ellis.data$V14
# vpd.ellis <- 0.6108 * exp(17.27 * t.max/(t.max+237.3)) - 0.6108 * exp(17.27 * t.min/(t.min+237.3))
# result.ellis <- lai_ppt.func(vpd = vpd.ellis,rain.test=rain.ellsi,Rlight  = 0.5,Rdark  = 1)
# plot.ellis.func("ellis lai (vpd on t).pdf")
# plot(vpd.ellis~rain.ellsi)
# a <- seq(1,2,0.1)
# b <- seq(1,2,0.1)
# plot(a~b,ylab=expression(paste("LAI","  ","(m"^"2"," ","m"^"-2",")")))


