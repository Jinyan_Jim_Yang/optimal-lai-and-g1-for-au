fun.parallel.func <- function(VPD.de.m,
                              map.de.m,
                              tmax.de.m,
                              par.de.m,
                              Ca=375){
  
  local.f <- g1.opt.func
  
  VPD.20.mean.m <- VPD.de.m
  VPD.20.mean <- as.vector(VPD.20.mean.m)
  rain.20.mean <- as.vector(map.de.m)
  tmax.20.mean <- as.vector(tmax.de.m)
  par.20.mean <- as.vector(par.de.m)
  
  # remove na for faster processing
  vpd.index <- which(is.na(VPD.20.mean) == FALSE)
  rain.index <- which(is.na(rain.20.mean) == FALSE)
  tmax.index <- which(is.na(tmax.20.mean) == FALSE)
  par.index <- which(is.na(par.20.mean) == FALSE)
  
  all.index <- Reduce(intersect, list(vpd.index,rain.index,tmax.index,par.index))
  # make inputes into data frame
  input.df <- data.frame(VPD.20.mean[all.index],
                         rain.20.mean[all.index],
                         tmax.20.mean[all.index],
                         par.20.mean[all.index])
  
  # set core number
  ifelse(detectCores() <= 2,ncore <- detectCores(),ncore <- detectCores() - 2)
  # split inputs
  temp <- split(input.df,as.numeric(gl(ncore,nrow(input.df)/ncore,nrow(input.df)))) 
  
  #setup parallel backend to use 8 processors
  cl <- makeCluster(ncore)
  registerDoParallel(cl)
  
  #start time
  strt<-Sys.time()
  writeLines("start")
  #loop
  result.ls <- list()
  ls <- list()
  ls <- foreach(j=1:ncore,.combine='c') %dopar% {
    input <- temp[[j]]
    for (i in 1:nrow(input)){
      # input.df:1 - VPD 2- rain 3- tmax 4-par
      vpd <- input[i,1]
      e <- input[i,2]
      t.max <- input[i,3]
      par <- input[i,4]
      # result.array: 1 through 6
      # 1-g1; 2-LAI; 3-gs; 4-NCG; 5-LUE; 6-GPP
      # result.ls[[length(result.ls)+1]] <- try(g1_function.ITE(VPD = vpd,E = e,PAR = par,TMAX = t.max))
      result.ls[[i]] <- local.f(VPD = vpd,E = e,PAR = par,TMAX = t.max,Ca = Ca)
    }
    return(result.ls)
  }
  
  stopCluster(cl)
  print(Sys.time() - strt)
  
  # output file names
  file.Name <- c("g1", "LAI","Gs","NCE","LUE","GPP")
  
  # filled na values
  ls.na <- as.list(rep(NA,length(VPD.20.mean)))
  ls.with.na <- lapply(ls.na, function(x){x <- rep(NA,6)}) 
  
  for (i in 1:length(all.index)){
    ls.with.na[[all.index[i]]] <- ls[[i]]
  }
  
  # make an array to store results
  result.all.array <- array(unlist(ls.with.na), 
                            dim = c(length(file.Name),
                                    nrow(VPD.20.mean.m),
                                    ncol(VPD.20.mean.m)))
  
  return(result.all.array)
  print(Sys.time() - strt)
}