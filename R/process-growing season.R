source("load.R")
vpd.sub <- readRDS(file.path("cache","vpdsub.rds"))

which(is.na(vpd.sub) == TRUE)


dir.create(file.path("downloads", "tmin GS"), showWarnings=FALSE)

sim_years <- 1991:2011
fullNameList <- c("air_temperature")


sunset.var.func <- function(year,
                     path.in = file.path("downloads","tmin"),
                     var.name = "tmin",
                     path.out = file.path("downloads","tmin GS")){
  
  files <- list.files(path = path.in, pattern = paste0(var.name,year))
  files <- files[grep("[.]nc", files)]
  temp <- list()
  
  for (i in 1:12){
    ncR <- nc_open(file.path(path.in,files[i]))
    temp[[i]] <- ncvar_get(ncR, "air_temperature")
    temp[[i]][which(is.na(vpd.sub) == TRUE)] <- NA
    month <- 
    saveRDS(temp[[i]],file.path(path.out,sprintf("%sGS.mon%i.rds",files[i],i)))
  }
  
  # array <- abind(temp, along=3)
  
  # Annual average VP for every x,y location
  # annual <- rowMeans(array, dims=2)
  
  # return(array)
}

# sunset.var.func(2011)

for (year in sim_years){
  sunset.var.func(year)
}

# tmin.fn <- list.files(path = file.path("downloads","tmin GS"), pattern = "tmin")
# 
# tmin.ls <- list()
# for (i in 1:length(tmin.fn)){
#   tmin.ls[[i]] <- readRDS(file.path("downloads","tmin GS",tmin.fn[i]))
# }
# 
# str_sub(tmin.fn[1],5,10)

test <- readRDS(file.path("downloads","tmin GS",(list.files(path = file.path("downloads","tmin GS"), pattern = ".mon1.rds")[1])))




tmin.array <-array(NA,dim = c(nrow(test),ncol(test),21))

tmin.fn <- list.files(path = file.path("downloads","tmin GS"), pattern = ".mon1.rds")


for (i in 1:length(tmin.fn)){
  tmin.array[,,i] <- readRDS(file.path("downloads","tmin GS",tmin.fn[i]))
}

average <- rowMeans(tmin.array, dims=2)

plot(raster(t(average[which((average) > 15)])))





for (year in sim_years){

    basefile <- paste0("tmin",year,".nc")
    fileName <- file.path("downloads","tmin",basefile)
    fileout <- file.path("downloads","tmin GS", gsub("[.]nc","GS.rds", basefile))
    
    if(!file.exists(fileout)){
      tmpvar <- open_variable_eMAST(fileName, fullNameList)
      saveRDS(tmpvar, fileout)
    } else {
      message("Skipped - ", fileout, "; already exists")
    }
}

for (i in seq_along(variableList)){
  for (year in sim_years){
    
    basefile <- paste0(variableList[i],year,".nc")
    fileName <- file.path("downloads",variableList[i],basefile)
    fileout <- file.path("cache", gsub("[.]nc",".rds", basefile))
    
    if(!file.exists(fileout)){
      tmpvar <- open_variable_eMAST(fileName, fullNameList[i])
      saveRDS(tmpvar, fileout)
    } else {
      message("Skipped - ", fileout, "; already exists")
    }
    
  }
}

getMean <- function(variable, path="cache/"){
  files <- list.files(path = path, pattern = paste0(variable))
  files <- files[grep("[.]rds", files)]
  temp <- list()
  
  for (i in 1:length(files)){
    temp[[i]] <- readRDS(file.path(path,files[i]))
  }
  
  temp.array <- abind(temp, along=3)
  
  # Annual average VPD for every x,y location
  average <- rowMeans(temp.array, dims=2)
  
  return(average)
}

get20mean <- function(var.name){
  file.name <- sprintf("%s1991_2011.rds",var.name)
  if(!file.exists(file.path("cache",file.name))){
    rain.mean <- getMean(var.name)
    saveRDS(rain.mean, file.path("cache",file.name))
  } else {
    message(file.name,"; already exists.")
  }
}
