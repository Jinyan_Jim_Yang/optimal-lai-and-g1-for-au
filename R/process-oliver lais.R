source("load.r")
# read in globcarbon lai
lai.glob <- get.glob.lai.func(fn = "downloads/globcarbon lai/GLO_V01_2011.LAI",
                              laicsv = "downloads/globcarbon lai/GLO_V01_2011.LAI.csv")
lai.glob.sum <-  summaryBy(. ~ site, data=lai.glob, FUN=mean, na.rm=T, keep.names=TRUE)
# read cyclopes lai
lai.cyc <- get.glob.lai.func(fn = "downloads/CYCLOPES/CYC_V31_2011.LAI",
                            laicsv = "downloads/CYCLOPES/CYC_V31_2011.LAI.csv")

lai.cyc.sum <-  summaryBy(. ~ site, data=lai.cyc, FUN=mean, na.rm=T, keep.names=TRUE)

# read geoland lai
lai.geo <- get.glob.lai.func(fn = "downloads/geoland/GEO_V01_2012.LAI",
                             laicsv = "downloads/geoland/GEO_V01_2012.LAI.csv")

lai.geo.sum <-  summaryBy(. ~ site, data=lai.geo, FUN=mean, na.rm=T, keep.names=TRUE)

get.model.lai.func <- function(lat,lon){
  
  lati <- ncvar_get(nc_open("downloads/pavg/pavg2002.nc"), "latitude")
  long <- ncvar_get(nc_open("downloads/pavg/pavg2002.nc"), "longitude")
  LAI.model <- readRDS("output/data/LAI1991_2011.rds")  
  
  col.lati <- c()
  for (i in 1:length(lat)){
    col.lati[i] <- which(abs(lat[i]-lati)==min(abs(lat[i]-lati)))
  }
  row.long <- c()
  for (i in 1:length(lon)){
    row.long[i] <- which(abs(lon[i]-long)==min(abs(lon[i]-long)))
  }
  
  LAI.model.sub <- c()
  for (i in 1:length(row.long)){
    LAI.model.sub[i] <- LAI.model[row.long[i] ,col.lati[i]]
  }
  return(LAI.model.sub)
}

lai.glob.sum$lai.modelled <- get.model.lai.func(lai.glob.sum$lat,lai.glob.sum$lon)

lai.cyc.sum$lai.modelled <- get.model.lai.func(lai.cyc.sum$lat,lai.cyc.sum$lon)

lai.geo.sum$lai.modelled <- get.model.lai.func(lai.geo.sum$lat,lai.geo.sum$lon)

pdf("compare lai products with model.pdf",width = 10,height = 7)
  par(mar=c(5,5,5,5))
  # plot globcarbon
  plot(lai.glob.sum$LAI~lai.glob.sum$modis,
       xlim=c(0,1.2),ylim=c(0,1.2),cex=0.8,
       xlab="20-year-average MODIS",ylab="LAI averaged from other sources",
       pch="")
  abline(a=0,b=1,lty="dashed",col="black")
  par(new=TRUE)
  plot(lai.glob.sum$lai.modelled~lai.glob.sum$modis,
       xlim=c(0,1.2),ylim=c(0,1.2),
       xlab=" ",ylab=" ",cex=0.8,
       pch=16,col="darkseagreen",
       ann = FALSE, axes=FALSE)
  # plot cyc
  par(new=TRUE)
  plot(lai.cyc.sum$LAI~lai.cyc.sum$modis,
       xlim=c(0,1.2),ylim=c(0,1.2),
       xlab=" ",ylab=" ",cex=0.8,
       pch=16,col="deepskyblue",
       ann = FALSE, axes=FALSE)
  
  par(new=TRUE)
  plot(lai.cyc.sum$lai.modelled~lai.cyc.sum$modis,
       xlim=c(0,1.2),ylim=c(0,1.2),
       xlab=" ",ylab=" ",cex=0.8,
       pch=16,col="darkseagreen",
       ann = FALSE, axes=FALSE)
  # plot geo
  par(new=TRUE)
  plot(lai.geo.sum$LAI~lai.geo.sum$modis,
       xlim=c(0,1.2),ylim=c(0,1.2),
       xlab=" ",ylab=" ",cex=0.8,
       pch=16,col="navy",
       ann = FALSE, axes=FALSE)
  
  par(new=TRUE)
  plot(lai.geo.sum$lai.modelled~lai.geo.sum$modis,
       xlim=c(0,1.2),ylim=c(0,1.2),
       xlab=" ",ylab=" ",cex=0.8,
       pch=16,col="darkseagreen",
       ann = FALSE, axes=FALSE)
  
  legend("topleft",legend = c("CYCLOPES","GEOLAND","Modelled"),
         pch=16,col=c("deepskyblue","navy","darkseagreen"),
         bty='n')
 dev.off() 
  