get.glob.lai.func <- function(fn,laicsv){

  olivexls <- "downloads/globcarbon lai/olive_datasets.xls"
  
  met <- readLines(laicsv)
  
  dates <- as.Date(as.numeric(str_trim(strsplit(met[1], ",")[[1]])), origin="0000-1-1")
  sites <- as.numeric(str_trim(strsplit(met[2], ",")[[1]]))
  
  r <- readBin(fn, what="integer", n=49*49*2*length(dates)*length(sites), size=1, signed=F)
  
  # reshape into matrix, each column a site
  m <- matrix(r, ncol=length(sites))
  
  # Loop through sites
  lai <- list()
  
  for(i in 1:ncol(m)){
    
    x <- m[,i]
    
    # delete flags by reshaping into matrix
    ms <- matrix(x, nrow=49^2)
    
    # flags are now in even columns; delete
    deli <- seq(2, ncol(ms), by=2)
    ms <- ms[,-deli]
    
    # 255 is missing value, values encoded by multiplying with 25.
    meanlai <- function(x)mean(x[x != 255])/25
    
    lai[[i]] <- data.frame(site=sites[i], LAI=apply(ms, 2, meanlai), Date=dates)
    
  }
  
  lai <- do.call(rbind,lai)
  
  # get lat/long
  belip2 <- read_excel(olivexls,2)
  dirsit <- read_excel(olivexls,3)[,1:3]
  
  names(belip2) <- names(dirsit) <- c("site","lat","lon")
  
  dirsit <- subset(dirsit, !is.na(site))
  
  belip2$site <- 1000 + belip2$site
  
  sitdf <- rbind(belip2, dirsit)
  
  lai <- merge(lai, sitdf, all=TRUE)
  
  lai.aust <- lai[which(lai$lat < -9 & lai$lon > 112 & lai$lon < 150),]
  
  lai.aust <- na.omit(lai.aust)
  
  lai.aust$modis <- get.modis.for.coord.func(long.t = lai.aust$lon, lati.t = lai.aust$lat)
#   # map
#   laim <- summaryBy(. ~ site, data=lai, FUN=mean, na.rm=T, keep.names=TRUE)
#   
#   lai.m.aust <- laim[which(laim$lat < -9 & laim$lon > 112 & laim$lon < 150),]
#   lai.m.aust <- na.omit(lai.m.aust)
#   
  # lai.m.aust$modis <- get.modis.for.coord.func(long.t = lai.m.aust$lon, lati.t = lai.m.aust$lat)
#   par(mar=c(5,5,5,5))
#   plot(lai.m.aust$LAI~lai.m.aust$modis,
#        xlim=c(0,1.2),ylim=c(0,1.2),
#        xlab="MODIS",ylab="GLOBCARBON",
#        pch=16,col="coral")
#   abline(a=0,b=1,lty="dashed",col="coral")
#   
#   library(maps)
#   map("world")
#   laim$LAIbin <- cut(laim$LAI, seq(0,5,length=10))
#   cols <- colorRampPalette(c("red","blue"))(10)
#   with(laim, points(lon, lat, pch=19, col=cols[LAIbin]))
#   with(lai.m.aust, points(lon, lat, pch=19, col=cols[LAIbin]))
  return(lai.aust)
}



# 
# write.csv(lai, "GLOBCARBON_LAI_tidy.csv", row.names=FALSE)

