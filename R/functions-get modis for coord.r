get.modis.for.coord.func <- function(long.t,lati.t){
  
#   modis.raw <- t(get.modis.mean())
#   modis <- modis.raw * 0.1
# 
# # and get the data to align with our outputs
# long.modis <- readMat("modis/long.mat")$long
# lati.modis <- readMat("modis/lati.mat")$lati
lati <- ncvar_get(nc_open("downloads/pavg/pavg2002.nc"), "latitude")
long <- ncvar_get(nc_open("downloads/pavg/pavg2002.nc"), "longitude")
# 
# find closest value
num.lati <- c()
for (i in 1:length(lati.t)){
  num.lati[i] <- which(abs(lati-lati.t[i]) == min(abs(lati - lati.t[i])))
}

num.longi <- c()
for (i in 1:length(long.t)){
  num.longi[i] <- which(abs(long-long.t[i]) == min(abs(long - long.t[i])))
}

# col.lati <- c()
# for (i in 1:length(lati)){
#   col.lati[i] <- which(abs(lati.modis-lati[i])==min(abs(lati.modis-lati[i])))
# }
# 
# row.long <- c()
# for (i in 1:length(long)){
#   row.long[i] <- which(abs(long.modis-long[i])==min(abs(long.modis-long[i])))
# }

modis.sub <- readRDS(file.path("modis","submean.rds"))

modis.ellis <- c()
for (i in 1:length(num.longi)){
  modis.ellis[i] <- modis.sub[num.longi[i],num.lati[i]]
}
  return(modis.ellis)
}


