library(ncdf4)
lin.data <- read.csv("WUEdatabase_merged_Lin_et_al_2015_NCC.csv")
library(plantecophys)
lin.data$g1 <- (lin.data$Cond * lin.data$CO2S / 1.6 / lin.data$Photo - 1) * sqrt(lin.data$VPD)
lin.aust <- lin.data[lin.data$latitude < 0,]
lin.aust <- lin.aust[grep("Eucalyptus",lin.aust$Species),]

lin.aust$Location <- as.character(lin.aust$Location)
lin.aust.list <-split(lin.aust, lin.aust[,c('Location')])
lin.g1.list <- lapply(lin.aust.list,
                      function(x){fitBB(x,
                                        varnames = list(ALEAF = "Photo", GS = "Cond",VPD = "VPD", Ca ="CO2S"),gsmodel="BBOpti")})

lin.g1.vec <- sapply(lin.g1.list,function(x)x$coef[[2]])

# coords
lati <- ncvar_get(nc_open("downloads/pavg/pavg2002.nc"), "latitude")
long <- ncvar_get(nc_open("downloads/pavg/pavg2002.nc"), "longitude")

# find closest value
num.lati <- c()
for (i in 1:length(lin.aust$latitude)){
  num.lati[i] <- which(abs(lati-lin.aust$latitude[i])==min(abs(lati-lin.aust$latitude[i])))
}

num.longi <- c()
for (i in 1:length(lin.aust$longitude)){
  num.longi[i] <- which(abs(long-lin.aust$longitude[i])==min(abs(long-lin.aust$longitude[i])))
}

g1.model <- readRDS(file.path("output","data","g11991_2011.rds"))
g1.vec <-c()
for (i in 1:length(num.longi)){
  g1.vec[i] <- g1.model[num.longi[i],num.lati[i]]
}

lin.aust$g1.model <- g1.vec

library(doBy)
g1.sum <- summaryBy(g1.model ~ Location,data = lin.aust,FUN = mean)
g1.sum$g1.lin <- lin.g1.vec
g1.sum <- g1.sum[!is.na(g1.sum$g1.model.mean),]
with(g1.sum,plot(g1.model.mean~g1.lin,xlim=c(0,4.5),ylim=c(0,4.5)))
abline(a=0,b=1,lty = 2)
fit <- lm(g1.sum$g1.model.mean~g1.sum$g1.lin)
abline(fit)
summary(fit)